const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser'); //bodyParser nos permite reicibir parametros por POST
const mysqlConnection = require('../../database.js');
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });



// Valida un usuario
router.post('/login/', (req, res) => {
    const { email, password} = req.body;
    console.log(req.body);
    let sql = 'SELECT * from usuarios where email = ? AND password = ?';
    var valores = [email, password];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            console.log(rows);
            if (rows.length > 0) {
                res.json({ 
                    ok: true,
                    // resp: rows[0],
                    mensaje: 'Login correcto'
                });
            } else {
                res.json({ 
                    ok: false,
                    mensaje: 'Usuario o password incorrecto...'
                 });
            }           
        } else {
            res.json({ ok: false });
            console.log(err);
        }
    });
});








module.exports = router;