const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser'); //bodyParser nos permite reicibir parametros por POST
const mysqlConnection = require('../../database.js');
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });




// GET all eventos
router.get('/', (req, res) => {
    mysqlConnection.query('SELECT * FROM eventos', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});


router.get('/evento', async function(req, res) {   
    const { id } = req.body;
    console.log(id);
    let sql ='SELECT * FROM eventos WHERE id_eventos = ?';
    var valores = [id];
    mysqlConnection.query(sql, valores, async(err, rows, fields) => {
        if (!err) {            
            res.json({
                ok: true,
                pedidos: rows[0]                
            });
        } else {
            res.status(400).json({
                ok: false,
                mensaje: 'Hubo un problema para generar la respuesta'
            });
        }
    });
});



// GET eventos por email de usuario
router.get('/mail', (req, res) => {
    const { email } = req.body;
    mysqlConnection.query('CALL PROC_CONSULTAR_EVENTOS_POR_EMAIL(?)', [email], (err, rows, fields) => {
        if (!err) {
            res.json(rows[0]);
        } else {
            console.log(err);
        }
    });
});

// DELETE An Evento
router.delete('/evento/', (req, res) => {
    const { id } = req.body;
    console.log(id);
    mysqlConnection.query('DELETE FROM eventos WHERE id = ?', [id], (err, rows, fields) => {        
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
        }
    });
});


// INSERT An Evento
router.post('/evento/', urlencodedParser, (req, res) => {
    const { titulo, descripcion, precio, id_usuarios } = req.body;

    let sql = 'INSERT INTO eventos(titulo, descripcion, precio, id_usuarios) VALUES (?,?,?,?)';
    var valores = [titulo, descripcion, precio, id_usuarios];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
        }
    });
});


// INSERT An Inscripcion
router.post('/evento/inscripcion', urlencodedParser, (req, res) => {
    const { id_evento, nombre, apellido, dni, celular, codigo_pago, codigo_inscripcion } = req.body;


    let sql = 'INSERT INTO inscripciones(id_evento, nombre, apellido, dni, celular, codigo_pago, codigo_inscripcion) VALUES (?,?,?,?,?,?,?)';
    var valores = [id_evento, nombre, apellido, dni, celular, codigo_pago, codigo_inscripcion];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
        }
    });
});


module.exports = router;